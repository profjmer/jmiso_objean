﻿# jmISO_Objean
## Présentation de l'Objean
* Derniers ajouts au projet
1. Uniformisation de la navigation
1. Uniformisation des noms des fichiers
1. Choix du canal WiFi
1. Module de gestion de l'afficheur TFT SPI
* Présentation de l'afficheur TFT couleur
* Présentation du fonctionnement javascript avec FireFox
* npp et nppFTP pour l'édition des pages et scripts
* Présentation du matériel livrable

##### Documents inclus dans jmISO_jmObjean.git
jmObjean 2019.pdf 

## Note
flash_download_tools 3.6.4 et 3.6.7 ne flash pas correctement l'ESP8266.

Lorsque le fureteur demande de servir une page, l'ESP8266 reset.

Toutefois en programmant à l'aide d'esptool, l'application fonctionne telle que démontré dans le document jmObjean 2019.pdf et de la vidéo: Réaliser un objet intelligent jmISO_Objean

esptool est installé dans le conteneur, mais il n'y a pas de partage entre les ports de communication COM de Windows et ceux de Linux dans le conteneur.

Comme j'ai également l'installation de l'environnement de développement sur mon PC, c'est de cette façon que j'ai programmé l'ESP.

Consulter la vidéo:https://youtu.be/1G5tsmtDYb0





	





	
