//#include <user_config.h>
#include <SmingCore/SmingCore.h>
#include "station.h"	
#include "AppSettings.h"

BssList networks;
String network, password;
Timer connectionTimer;
int essai=0;

void initStation(){
	AppSettings.load();

	WifiStation.enable(true);

	if (AppSettings.exist())
	{
		WifiStation.config(AppSettings.ssid, AppSettings.password);
		if (!AppSettings.dhcp && !AppSettings.ip.isNull())
			WifiStation.setIP(AppSettings.ip, AppSettings.netmask, AppSettings.gateway);
	}

	WifiStation.startScan(networkScanCompleted);

	WifiStation.waitConnection(connectOk, 30, connectFail); // We recommend 20+ seconds at start
}


void networkScanCompleted(bool succeeded, BssList list)
{
	if (succeeded)
	{
		for (int i = 0; i < list.count(); i++)
			if (!list[i].hidden && list[i].ssid.length() > 0)
				networks.add(list[i]);
	}
	networks.sort([](const BssInfo& a, const BssInfo& b){ return b.rssi - a.rssi; } );
}

// Will be called when WiFi station was connected to AP
void connectOk()
{
	Serial.println("Station IP");
	Serial.println(WifiStation.getIP().toString());
	WifiAccessPoint.enable(false); // fermeture du point d'accès
}

// Will be called when WiFi station timeout was reached
void connectFail()
{
	essai++;
	Serial.printf("essai %d\n",essai);
	Serial.println("Station inactive!");

	if (essai >4)
	{
			Serial.println("Fermeture de la station!, activation AP");
			WifiStation.enable(false); 
	}
	else 	WifiStation.waitConnection(connectOk, 5, connectFail); // Repeat and check again
}