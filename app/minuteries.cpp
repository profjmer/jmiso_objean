// minuteries.cpp
#include "gestionDHT22.h"
#include "gestionDS18S20.h"
#include "gestionOLED.h"
#include "gestionBME280.h"
#include "gestionRTC_NTP.h"
#include <stdio.h>
#include "webserver.h"
#include "gestionILI9341.h"

// minuteries de l'objet
Timer timerOLED, timerDS1820, timerBME280, timerDHT22, timerTFT, timerTFTsim;

bool tftSimulation =true;

String timerMenu(){
    return "\ntimer menu\ntimer dhtstop\ntimer dhtstart\ntimer dsstop\ntimer dsstart\ntimer bmestop\ntimer bmestart\ntimer tftsim\ntimer stop\ntimer start\n";
}

/**
 * @brief Toggle de la simulation sur TFT
 * 
 */
void tftSim(bool sim){
    if(sim){
        timerTFTsim.restart();
    }
    else{
        timerTFTsim.stop();
    }
} 

// rafraichissement du OLED
void OLED(){
    setZone4(ts());
    updateOLED();
}

// nouvelle mesure du DHT22
void lectureBME(){
    BME280Lire (); // lire mais format réponse trop grand pour OLED
    setZone3("  " + BME280Last()); // format réduit pour OLED
    wsSend(BME280JSON());
}

void lectureDS1820(){
    setZone2(DS1820Lire());
    wsSend(DS1820JSON());
}

void lectureDHT22(){
    setZone2(DHT22Lire());
    //Serial.println(DHT22Lire()); // effectue une conversion et transmet au port série
    wsSend(DHT22JSON()); // envoi aux clients websockets en format json
}

String timerBME280stop(){
    timerBME280.stop();
    return "Arrêt minuterie BME280\n";
}

String timerBME280restart(){
    timerBME280.restart();
    return "Redémarrage minuterie BME280\n";
}

String timerDHT22stop(){
    timerDHT22.stop();
    return "Arrêt minuterie DHT22\n";
}

String timerDHT22restart(){
    timerDHT22.restart();
    return "Redémarrage minuterie DHT22\n";
} 

String timerDS1820stop(){
    timerDS1820.stop();
    return "Arrêt minuterie DS1820\n";
}

String timerDS1820restart(){
    timerDS1820.restart();
    return "Redémarrage minuterie DS1820\n";
}

String timersStop(){
    timerDHT22.stop();
    timerBME280.stop();
    timerDS1820.stop();
    timerTFTsim.stop();    
    return "Arrêt des timers\n";
}

String timersRestart(){
    timerDHT22.restart();
    timerBME280.restart();
    timerDS1820.restart();
    timerTFTsim.restart();
    return "Redémarrage des timers\n";
}

void timerInit(){
    timerOLED.initializeMs(1000, OLED).start();
    timerBME280.initializeMs(6000, lectureBME).start();
    timerDS1820.initializeMs(5000, lectureDS1820).start();
    timerDHT22.initializeMs(1500, lectureDHT22).start();
    timerTFTsim.initializeMs(500, simulation).start();    
}

String timerInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return timerMenu();		
	}

 	if(qteMots == 2){
		if(Mot[1]== "dhtstart")	return timerDHT22restart();	
	} 

    if(qteMots == 2){
		if(Mot[1]== "dhtstop")	return timerDHT22stop();	
	} 

	if(qteMots == 2){
		if(Mot[1]== "bmestart")	return timerBME280restart();	
	}

	if(qteMots == 2){
		if(Mot[1]== "bmestop")	return timerBME280stop();	
	}    

 	if(qteMots == 2){
		if(Mot[1]== "dsstart")	return timerDS1820restart();	
	}
 
	if(qteMots == 2){
		if(Mot[1]== "dsstop")	return timerDS1820stop();	
	}

	if(qteMots == 2){
		if(Mot[1]== "start")	return timersRestart();		
	}

    if(qteMots == 2){
		if(Mot[1]== "stop")	return timersStop();
	}

    if(qteMots == 2){
		if(Mot[1]== "tftsim"){  // bascule la simulation
            tftSimulation = !tftSimulation;
            Serial.printf("sim %d\n",tftSimulation);
            tftSim(tftSimulation);
            if(tftSimulation)return "TFT sim on\n";
            else return "TFT sim off\n";
        }
	}

    return line + ("???\n");	    
}