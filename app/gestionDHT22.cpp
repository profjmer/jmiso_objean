// gestionDHT22.cpp
#include <SmingCore/SmingCore.h>
#include "gestionDHT22.h"
#include "gestionRTC_NTP.h"
#include <WString.h>

#define DHT_PIN 12


// tableau pour sauvegarder les valeurs
#define qte 24
float ttemperature[qte], thumidity[qte];
String ttemps[qte];

String DHT22pin(){
	return "DHT22 pin "+String(DHT_PIN,10)+"\n";	
}

// Pousse les dernières valeurs du capteurs dans les tableaux
// La plus ancienne valeur des tableaux est écrasée
// Les nouvelles valeurs sont plaçées à la fin
void toHistorique(String Temps, float Temperature, float Humidity)
{
	int i;
	for(i=0;i<qte-1;i++){
		ttemps[i]= ttemps[i+1];
		ttemperature[i]=ttemperature[i+1];
		thumidity[i]=thumidity[i+1];
	}
	ttemps[i]= Temps;
	ttemperature[i]=Temperature;
	thumidity[i]=Humidity;
	//printHistorique();
}

String DHT22HistoJSON(){
	int i;
	String rep = "{\"type\":\"dht22histo\",\"ts\":[\"";
	for(i=0;i<qte;i++){
		rep += ttemps[i];
		if(i<qte-1)rep+="\",\"";
	}
	rep+="\"],\"T\":[";
	
	for(i=0;i<qte;i++){
		rep += String(ttemperature[i],1);
		if(i<qte-1)rep+=",";
	}
	rep+="],\"HR\":[";
	
	for(i=0;i<qte;i++){
		rep += String(thumidity[i],0);
		if(i<qte-1)rep+=",";		
	} 

	rep+="]}";  
	return rep;
	
}

// structure pour sauver la lecture du capteur
TempAndHumidity th; 
// temps de la prise de la mesure
String dht22Time;

DHT dht(DHT_PIN,DHT22);

void DHT22init(){
	dht.begin();
}

float dht22Temp(){return th.temp;}
float dht22HR(){return th.humid;}

/**
 * @brief Effectue une conversion
 * 
 * @return String  Les valeurs en format JSON avec un time stamp
 */
String DHT22JSON(){
	String rep = "{\"type\":\"dht22\",\"ts\":\""+ts()+"\",\"T\":" +String(th.temp,1)+",\"HR\":" +String(th.humid,0)+"}\n";
    
	return rep;
}

String DHT22Last(){
		String T(th.temp,1);
		String RH(th.humid,0);
		String rep = ("T "+T +"  HR " + RH);
		return rep;
}

String DHT22Lire(){
	bool simulation =true;
	
	dht22Time = readRTC();	// timeStamp de la conversion
	

	if(simulation){
		th.temp = -50 + random(100);
		th.humid = random(100);

		String T(th.temp,1);
		String RH(th.humid,0);
		String rep = ("T "+T +"  HR " + RH + "\n");

		toHistorique(ts(),th.temp, th.humid);

		return rep;
	}

	if(dht.readTempAndHumidity(th))
	{
		// Lecture du capteur DHT22
		String T(th.temp,1);
		String RH(th.humid,0);

		String rep = ("T "+T +"  HR " + RH + "\n");
		return rep;
	}
	else{
		int error = dht.getLastError();
		if(error == errDHT_Timeout){
			return 	("DHT timeout\n");
		}
		if(error == errDHT_Checksum){		
			return("DHT checksum\n");
		}
		return ("DHT erreur\n");
	}
}

String DHT22Menu(){
	return "\ndht menu\ndht lire\ndht last\ndht json\ndht pin\n";
}

String DHT22Interpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return DHT22Menu();		
	}

	if(qteMots == 2){
		if(Mot[1]== "last")	return DHT22Last();		
	}

	if(qteMots == 2){
		if(Mot[1]== "lire")	return DHT22Lire();		
	}

	if(qteMots == 2){
		if(Mot[1]== "json")	return DHT22JSON();		
	}
	
	if(qteMots == 2){
		if(Mot[1]== "histo")	return DHT22HistoJSON();		
	}
	
	if(qteMots == 2){
		if(Mot[1]== "pin")	return 	DHT22pin();
	}
	
    return line + ("???\n");		
}