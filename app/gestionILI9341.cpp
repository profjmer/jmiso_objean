#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>
#include <Libraries/Adafruit_ILI9341/Adafruit_ILI9341.h>

//extern uint16_t MAX_X, MAX_Y ;          // display screen size (TFT.c)

String tftMenu(){return "\ntft menu\ntft grille\ntft in (y1 y2 y3)\ntft clear\ntft inv\ntft normal\ntft rot0\ntft rot180\n";}

#define points 256
Adafruit_ILI9341 tft;
uint8_t graphe = 1; // orientation du graphe
uint8_t legende =0;	// orientation de la légende
uint16_t nPoints=0;
uint8_t trace1[points];
uint8_t trace2[points];
uint8_t trace3[points];
char Temperature[16];
char Humidity[16];
uint8_t up =1;
uint8_t v =0;

void AfficheGrille();
String grapheIn(uint8_t y1, uint8_t y2, uint8_t y3);
int gain_decal(int y, int gain, int decal);

void tftInit(){
	tft.begin();
	AfficheGrille();
}


// Trace deux point sur un graphe et pousse les anciennes valeurs vers la gauche
// pour afficher les nouvelles valeurs
// la zone de du graphe est 200 pixels de hauteur partant du coin sup�rieur droit
// Affiche les valeurs les plus r�centes � l'�cran.
String grapheIn(uint8_t v1, uint8_t v2, uint8_t v3)
{
  int y1,y2,y3;
	uint16_t j=0;

	tft.setRotation(graphe);
    
    //Selon l'�tendu et le d�calage des valeurs d'entr�es
    // les points sont calcul�s pour occup�s une �tendue de -200 et un d�calage de 200
    
    // �tendue temp�rature 50*C d�calage 0*C (zone de 0-50*C)
    // gain temp -200/50, d�calage 200
    y1 = gain_decal(v1, -2, 200); 
    
    // �tendue sonde RH 0-100%, d�calage 0%
    // gain sonde RH -200/100, 200
    y2 = gain_decal(v2, -4, 200);
    
	
	// �tendue sonde pression 10- 16 psi 
    // gain sonde  -200/10, 200
    y3 = gain_decal(v3, -1, 120);
	
    // limites graphiques
    if(y1>200 || y1<0 )return "Valeur y1 non valide\n";
    if(y2>200 || y2<0 )return "Valeur y2 non valide\n";
    if(y3>200 || y3<0 )return "Valeur y3 non valide\n";					
    
    
    // au reset les valeurs sont entr�es directement
    if(nPoints<points)
    {
        trace1[nPoints]=y1;
        trace2[nPoints]=y2;   
				trace3[nPoints]=y3;  
        tft.drawPixel(nPoints,y1,ILI9341_GREEN);
        tft.drawPixel(nPoints,y2,ILI9341_YELLOW);
	    	tft.drawPixel(nPoints,y3,ILI9341_BLUE);
        nPoints++;
    }
    else{        
        
		    tft.drawPixel(255,trace1[255],ILI9341_BLACK);
        tft.drawPixel(255,trace2[255],ILI9341_BLACK);
        tft.drawPixel(255,trace3[255],ILI9341_BLACK);				
		// d�calle les valeurs pour en ajouter une autre
        for(j=0;j<points-1;j++)
        {
            //efface les traces
            tft.drawPixel(j,trace1[j],ILI9341_BLACK);
            tft.drawPixel(j,trace2[j],ILI9341_BLACK);
            tft.drawPixel(j,trace3[j],ILI9341_BLACK);						
            // d�calle les donn�es
            trace1[j]=trace1[j+1];
            trace2[j]=trace2[j+1];
            trace3[j]=trace3[j+1];						
            // dessine les donn�es d�cal�es
            tft.drawPixel(j,trace1[j],ILI9341_GREEN);
            tft.drawPixel(j,trace2[j],ILI9341_YELLOW);
            tft.drawPixel(j,trace3[j],ILI9341_BLUE);						
        }
			
		// ajoute les nouvelles valeurs en fin de graphe
		trace1[points-1]=y1;
		trace2[points-1]=y2;
		trace3[points-1]=y3;		
		tft.drawPixel(points-1,y1,ILI9341_GREEN);
		tft.drawPixel(points-1,y2,ILI9341_YELLOW);
		tft.drawPixel(points-1,y3,ILI9341_BLUE);		
	}
        

	
	// efface zone affichage temp�rature
	//TFT_DrawString(Temperature,120,0,2,BLACK);   
	tft.setCursor(120,0);
	tft.setTextSize(2);
	tft.setTextColor(ILI9341_BLACK);
	tft.printf(Temperature);
	
	// efface ancienne valeur
	//TFT_DrawString(Humidity,120,30,2,BLACK);
	tft.setCursor(120,30);
	//tft.setTextSize(2);
	tft.setTextColor(ILI9341_BLACK);
	tft.printf(Humidity);
	
	
	// affiche derni�re valeur temp�rature
	sprintf(Temperature, "T %d *C", v1/2);
	tft.setCursor(120,0);
	//tft.setTextSize(2);
	tft.setTextColor(ILI9341_GREEN);
	tft.printf(Temperature); 
	
	// affiche derni�re valeur humidit�
	//sprintf(Humidity, "H %d%%", v2);
	//TFT_DrawString(Humidity,120,30,2,YELLOW);  
	sprintf(Humidity, "H %d%%", v2*2);
	tft.setCursor(120,30);
	//tft.setTextSize(2);
	tft.setTextColor(ILI9341_YELLOW);
 	tft.printf(Humidity);   
	
  return "Nouveaux points y1:"+String(y1)+" y2:"+String(y2)+" y3:"+String(y3)+"\n";
}

String tftIn(String line){
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	uint8_t y1 = strtol(Mot[2].c_str(),NULL,10);		
	uint8_t y2 = strtol(Mot[3].c_str(),NULL,10);			
	uint8_t y3 = strtol(Mot[4].c_str(),NULL,10);     
	return grapheIn(y1,y2,y3);	
}

int gain_decal(int y, int gain, int decal){
  return(y*gain + decal);
}

void AfficheGrille(){
  tft.fillScreen(0);
	int i;
	// Axe X Divisions
	tft.setRotation(graphe); // paysage
	tft.drawFastHLine(0,205,260,ILI9341_WHITE);
	for(i=260;i>=60;i-=60)   tft.drawFastVLine(i,201,4,ILI9341_WHITE);
	for(i=260;i>30;i-=30)    tft.drawFastVLine(i,203,2,ILI9341_WHITE);
    
  // Axe X unit�s
	tft.setCursor(18,220);
	tft.setTextColor(ILI9341_RED);
	tft.setTextSize(1);
  tft.printf("4         3         2         1        min");
    
	// Axe Y divisions
	tft.drawFastVLine(260,0,200,ILI9341_WHITE);
	for(i=0;i<200;i+=50)    tft.drawFastHLine(256,i,4,ILI9341_WHITE);
	for(i=0;i<200;i+=25)    tft.drawFastHLine(258,i,2,ILI9341_WHITE);
	
	
	// Axes Y L�gendes et unit�s
	tft.setRotation(legende);
	tft.setCursor(35,265);
	//tft.setTextSize(2);
	tft.setTextColor(ILI9341_GREEN);
	tft.printf("0       12       25      37     50");
	
	tft.setCursor(35,290);
	tft.printf("Temperature *C");
	
	tft.setCursor(35,275);
	tft.setTextColor(ILI9341_YELLOW);
	tft.printf("0       25       50      75    100");

	tft.setCursor(160,290);
	tft.printf("Humidite %");
	
	tft.setCursor(90,305);
	tft.setTextColor(ILI9341_BLUE);
	tft.printf("Pression kPa");
}

void simulation()
{
	tft.setRotation(graphe);
	if(up) // valeurs croissantes
	{
			grapheIn(v,v,v); // appel du graphe avec nouvelles valeurs pour les deux traces
			v++;
			if(v>50) up=0;
	}
	else // valeurs d�croissantes
	{
				grapheIn(v,v,v);
			v--;
			if(v<1) up=1;
	}
}


String tftInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return tftMenu();		
	}
	    
  if(qteMots == 2){
	  if(Mot[1]== "grille"){AfficheGrille();  return "Affichage grille fait\n";}	
	}

	if(qteMots == 2){
	  if(Mot[1]== "inv"){tft.invertDisplay(true);  return "inversion fait\n";}	
	}

	if(qteMots == 2){
	  if(Mot[1]== "normal"){tft.invertDisplay(false);  return "inversion fait\n";}	
	}

	if(qteMots == 2){
	  if(Mot[1]== "rot0"){  
			tft.fillScreen(0);
			graphe =1; legende =0;  
			AfficheGrille();
			return "rotation 0 fait\n";}	
	}
	
	if(qteMots == 2){
	  if(Mot[1]== "rot180"){
			tft.fillScreen(0);
			graphe =3; legende =2;  
			AfficheGrille();
			return "rotation 180 fait\n";}	
	}

  if(qteMots == 5){
		if(Mot[1]== "in")	return tftIn(line);	
	}


  if(qteMots == 2){
		if(Mot[1]== "clear"){
			tft.fillScreen(0);
			return "couleur TFT 0\n";
    }		
	}

  return line + ("???\n");	
}

