// gestionOLED.cpp
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include "application.h"
#include <Libraries/Adafruit_SSD1306/Adafruit_SSD1306.h>
#include "gestionOLED.h"
#include "gestionBME280.h"

Adafruit_SSD1306 display(-1); // reset Pin required but later ignored if set to False
String zone1, zone2, zone3, zone4;

String oledMenu(){
	return "\noled menu\noled zone message [zone est 3 ou 4 et le message est de 42 caractères maximum]\noled invert\noled normal\noled on\noled off\n";
}

#define SSD1306_I2C_ADDRES 0x3c
void OLEDinit(){
	display.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS, FALSE);
	setZone3("Zone 3");
	setZone4("Zone 4");
	updateOLED();
}

void setZone1(String s){zone1=s;}
void setZone2(String s){zone2=s;}
void setZone3(String s){zone3=s;}
void setZone4(String s){zone4=s;}

void showZone1(){
	display.setTextColor(WHITE);
	display.setTextSize(2);
	display.setCursor(10,0);
	display.println(String(version)); // située dans application.h
	display.setTextSize(1);
}

void showZone2(){
	display.setCursor(10,16);
	display.println(zone2);
}

void showZone3(){
	display.setCursor(0,32);
	display.println(zone3);
}

void showZone4(){
	display.setCursor(0,48);
	display.println(zone4);
}

void updateOLED(){
	display.clearDisplay();
	showZone1();
	showZone2();
	showZone3();
	showZone4();
	display.display();
}

#define lineMax 42	// correspond à deux lignes de grosseur 1
String oledInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return oledMenu();		

		if(Mot[1]== "on"){
			display.dim(false);
			return "oled on\n";
		}

		if(Mot[1]== "off"){
			display.dim(true);
			return "oled off\n";
		}			

		if(Mot[1]== "invert"){
		  display.invertDisplay(true);
		  return "oled mode invert\n";
		}

		if(Mot[1]== "normal"){
		  display.invertDisplay(false);
		  return "oled mode normal\n";
		}

	}

	if(qteMots >= 3){
		if(Mot[1]== "zone"){
			int noZone = strtol(Mot[2].c_str(),NULL,10); // identification du no de la zone d'affichage
			line.remove(0,12); // retire oled et numéro de zone du texte
			int qte = line.length(); // nombre de char	
			if(qte > lineMax)line.remove(lineMax, qte-lineMax); // tronque les longs messages
			
			bool zoneOk = false;
			// affiche dans la zone sélectionnée
			switch(noZone){
				case 3: setZone3(line);zoneOk=true;break;
				case 4: setZone4(line);zoneOk=true;break;
			}
			if(zoneOk){
				updateOLED();
				return "transmis vers OLED\n";
			}
			else return "zone interdite\n";
		}

	}
	
	return line + ("???\n");
}

