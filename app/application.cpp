#include <SmingCore/SmingCore.h>
#include "application.h"
#include "udpserver.h"	
#include "gestionIO.h"
#include "station.h"
#include "AppSettings.h"
#include "webserver.h"
#include "gestionI2C.h"
#include "gestionBME280.h"
#include "gestionOLED.h"
#include "gestionOneWire.h"
#include "gestionDS18S20.h"
#include "gestionDHT22.h"
#include "minuteries.h"
#include "gestionILI9341.h"


// exécuter lorsque le système est prêt
// initialise les modules
void initModules(){
	udpserver_start();
	initGestionIO();
	startWebServer();
	startFTP();
	initStation();
	I2Cinit();
	BME280init();
	OLEDinit();
	OneWireInit();
	DS18S20init();
	DHT22init();
	timerInit();
	tftInit();
}


#define canal 5
void init(){
	spiffs_mount();
	// Start AP for configuration
	WifiAccessPoint.enable(true);
	
	WifiAccessPoint.config(version, "", AUTH_OPEN, false, canal, 200);

	// Run WEB server on system ready
	System.onReady(initModules);
}
