/*************************************************************************
 * @file	gestionIO.c				 
 * @version	1.0
 * @date	2019 03 13
 * @brief 
 * Module de gestion des broches GPIOs
*/
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>
#include "webserver.h"

#define GPIO0 0
#define GPIO1 1
#define GPIO2 2
#define GPIO3 3
#define GPIO4 4
#define GPIO5 5

#define GPIO12 12
#define GPIO13 13
#define GPIO14 14
#define GPIO15 15
#define GPIO16 16

// Exemple pour utiliser des broches en output
// Définir le nombre et inscrire les gpios qui seront utilisées
// Dans la liste pin_list
#define nb_pins 0
int pin_list[nb_pins]={};

/** @brief		Identifie les broches GPIO disponibles
 * @param[in]	rien
 * @returns		Retourne une String donnant la liste des broches disponibles pour les commandes
 */
String pins(){
	int i;
	String pins= "pin_list: ";
	for(i=0;i<nb_pins;i++){
		pins += String(pin_list[i],10) +" ";
	}	
	return pins;
}

/** @brief		Initialise les broches utilisées en sortie.
 * @param[in]	rien
 * @returns		rien
 */
void initGestionIO()
{
	int i;
	for(i=0;i<nb_pins;i++){
		pinMode(pin_list[i], OUTPUT);
	}
}

/** @brief		Retourne le menu pour le module gestionIO.c
 * @param[in]	rien
 * @returns		Retourne une String donnant la liste des commandes et des broches disponibles
 */
String gpioMenu(){
	String menu = "gpio\ngpio write pin etat  " + pins() +"\n"+"gpio flip pin  " + pins() +"\n";
return menu;
}

/** @brief		Retourne l'état des GPIOs
 * @param[in]	rien
 * @returns		Retourne une String donnant l'état des GPIOs dans la liste pin_list
 */
String gpios()
{
	int i,pin;
	String io;
	String rep="GPIOS:\t";
	for(i=0;i<nb_pins;i++){
		pin=pin_list[i];
		if(digitalRead(pin)==0)io= " 0\t";
		else io = " 1\t";
		rep=rep+"GPIO "+pin+io;
	}
	rep +="\n";
	return rep;
}

/** @brief		Retourne l'état de la broche fournie en paramètre
 * @param[in]	numéro de la broche
 * @returns		Retourne une String donnant l'état de la broche
 */
String pinFeedback(uint8_t pin)
{	
		String id(pin);	// construire une String à partir du uint8_t
		String rep = "GPIO " + id;
		// feedback
		if(digitalRead(pin)==1)	return rep+" 1\n";
		else return rep + " 0\n";
}

/** @brief		Contrôle les sorties GPIOs
 * activation des sorties sur réception de la commande
 * la broche est relue et son etat est transmis au client
 * exemple de commande: gpio 12 0	 pour mettre gpio 12 à 0
 * @param[in]	numéro de la broche, état de la broche
 * @returns		Retourne une String donnant l'état de la broche
 */
String gpio(uint8_t pin, uint8_t etat)
{
		int i;	
		// si la pin n'est pas dans la liste -> return "pin not in pin_list\n";
		bool found = false;
		for(i=0;i<nb_pins;i++){
			if(pin==pin_list[i]){found=true;break;}
		}
		if(found==false)return "pin not in pin_list\n";
		
		// modifier etat
		// n'importe quelle valeur autre que 0 met la broche à 1
		if(etat==0)digitalWrite(pin,0);
		else digitalWrite(pin,1);
	
		return pinFeedback(pin);
}

/** @brief		Bascule une sortie GPIO
 * exemple de commande: gpio flip 12	 pour basculer GPIO12
 * @param[in]	numéro de la broche
 * @returns		Retourne une String donnant l'état de la broche
 */
// basculer l'état de la broche dont l'id est fournie en paramètre	
String flipio(uint8_t pin)
{
	int i;
	
	// si la pin n'est pas dans la liste -> return
	bool found = false;
	for(i=0;i<nb_pins;i++){
		if(pin==pin_list[i]){found=true;break;}
	}
	if(found==false)return"pin not in pin_list\n";

	// lire et écrire l'inverse.
	digitalWrite(pin,!digitalRead(pin));

	// feedback
	return pinFeedback(pin);
}

/**
 * @brief mise en format JSON du pinFeedback
 * 
 * @param id identification de la broche
 * @return String  {"type":"gpio", "id":x, "etat":y} x numéro pin y 0 ou 1
 */
String pinFeedbackToJSONstring(int id){
	bool etat = digitalRead(id);

	String rep ="{\"type\":\"gpio\", \"id\":" +(String)id + ", \"etat\":" + etat + "}";
	Serial.println(rep);
	return (rep);
}

/**
 * @brief Envoi le statut des GPIOs par websocket
 * 
 */
void statutGPIOS(){
	for(int i=0;i<nb_pins;i++){
		wsSend(pinFeedback(pin_list[i]));
		wsSend(pinFeedbackToJSONstring(pin_list[i]));
	}
}

/** @brief		Interpréteur de commandes du module de gestionIO
 * @param[in]	ligne de commande
 * @returns		Retourne une String donnant le résultat de l'interprétation de la commande
 */
String gpioInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	// retourne l'état des GPIOs dans la pinLIST
	if(line =="gpio")	return gpios();

	// retourne le menu des commandes du module gestionIO
	if(qteMots == 2){
		if(Mot[1]== "menu") return gpioMenu();
	}

	// exemple de commande gpio flip 2 pour basculer la broche 2
	if(qteMots == 3){
		if(Mot[1]== "flip"){
			int i;
			// extraire no de pin_list et convertir en uint8
			uint8_t pin = (int8_t)atoi(Mot[2].c_str());
			return flipio(pin); // changer l'état gpio et retourne la chaine du résultat
		}
	}
	
	// exemple de commande: gpio write 12 0	mettre gpio 12 à 0
	if(qteMots == 4){
		if(Mot[1]== "write")
		{
			// extraire no de pin_list et convertir en uint8
			uint8_t pin = (int8_t)atoi(Mot[2].c_str());		
			// extraire etat
			uint8_t etat=(int8_t)atoi(Mot[3].c_str());
			return gpio(pin,etat);	// changer l'état gpio qui retourne la chaine du résultat
		}
	}
}
