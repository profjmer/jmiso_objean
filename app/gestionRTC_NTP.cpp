// gestionRTC.c
// synchronise 
#include <user_config.h>
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>
#include <Libraries/DS3232RTC/DS3232RTC.h>
#include "gestionRTC_NTP.h"
#include "gestionI2C.h"
//#include <time.h>

String rtcMenu(){
	return "\nrtc menu\nrtc time\nrtc settime JJ MM AA HH MM SS\n";
}

void onNtpReceive(NtpClient& client, time_t timestamp);

NtpClient ntpClient ("ntp.cegepsherbrooke.qc.ca", 30, onNtpReceive);

void onNtpReceive(NtpClient& client, time_t timestamp) {
	Serial.printf("Synchro avec serveur NTP\n");
	SystemClock.setTime(timestamp, eTZ_UTC); // selon fuseau horaire
	DSRTC.set(timestamp); 
}

/**
 * @brief time stamp: le temps du RTC est plus juste alors on l'utilise
 * 
 * @return String représentant le temps sans \n 
 */
String ts(){
	// on lit directement les registres du RTC
	String temps = readI2C(0x68,0,7);

	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(temps, ' ' , Mot);
	int i;
	// ajoute leading 0 pour nombre 0-9
	for(i=0;i<7;i++){
		if(i==3)continue;
		else if(Mot[i].length()==1)Mot[i]="0"+Mot[i];
	}
	
	return "20"+Mot[6]+"-"+Mot[5]+"-"+Mot[4]+" "+Mot[2]+":"+Mot[1]+":"+Mot[0];
}

// le temps du RTC est plus juste
// alors on l'utilise
String readRTC(){
	// on lit directement les registres du RTC
	String temps = readI2C(0x68,0,7);

	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(temps, ' ' , Mot);
	int i;
	// ajoute leading 0 pour nombre 0-9
	for(i=0;i<7;i++){
		if(i==3)continue;
		else if(Mot[i].length()==1)Mot[i]="0"+Mot[i];
	}
		
	return "20"+Mot[6]+"."+Mot[5]+"."+Mot[4]+"  "+Mot[2]+":"+Mot[1]+":"+Mot[0]+"\n";
}

String writeRTC(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	uint8_t jour = strtol(Mot[2].c_str(),NULL,16);		
	uint8_t mois = strtol(Mot[3].c_str(),NULL,16);			
	uint8_t an = strtol(Mot[4].c_str(),NULL,16);
	uint8_t H = strtol(Mot[5].c_str(),NULL,16);		
	uint8_t M = strtol(Mot[6].c_str(),NULL,16);		
	uint8_t S = strtol(Mot[7].c_str(),NULL,16);	

	uint8_t octets[7]={S,M,H,0,jour,mois,an};
	uint8_t *pOctets = octets;

	// écrit dans le RTC adresseI2C, 0, le tableau
	String rep = writeI2Carray(0x68,0, pOctets, 7);
	return rep;
}

// Requête NTP
String reqNTP(){
	// Affiche les valeurs locales de la date et du temps
	SystemClock.setTimeZone(-5); // Montréal ?
	ntpClient.requestTime();
	return "Requete NTP\n";
}

String rtcInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

		if(qteMots == 2){
		if(Mot[1]== "menu")	return rtcMenu();		
	}
	
	if(qteMots == 2){
		if(Mot[1]== "time")	return readRTC();		
	}

	if(qteMots == 8){
		if(Mot[1]== "settime")	return writeRTC(line);		
	}

	return line + "???\n";

}
