/*************************************************************************
 * @file	routeur.c				 
 * @version	1.0
 * @date	2019 03 13
 * @brief
 		Module permettant de router les lignes de commandes recues des modules de communications
		vers les modules spécifiés par le premier mot de la ligne de commande.
		La réponse sous forme d'une String est retransmise vers le module de communication solliciteur.
*/
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include "application.h"
#include "gestionIO.h"
#include "gestionI2C.h"
#include "gestionRTC_NTP.h"
#include "gestionBME280.h"
#include "gestionOLED.h"
#include "gestionOneWire.h"
#include "gestionDS18S20.h"
#include "gestionDHT22.h"
#include "minuteries.h"
#include "conversions.h"
#include "gestionILI9341.h"


/** Commande List
 * @brief Liste les commandes disponibles du projet. 
 * list est une String contenant les menus des modules. Lors de l'ajout d'un module, on ajoute ici 
 * la fonction retournant la liste des commandes du module.
 * @param[in] Aucun 
 * @returns String contenant la liste des commandes disponibles du système.
 */
String list ="version\n" + gpioMenu()+ i2cMenu() +rtcMenu() + BME280Menu() +oledMenu()+ owMenu() + DS1820Menu() +DHT22Menu() +timerMenu() +conversionsMenu()+ tftMenu();

/** Commande route
 * @brief		Aiguilleur des lignes de commande et des réponses
 * @param[in]	Ligne de commande dont le premier mot est le module destinataire de la ligne de commande
 * ou bien une commande système
 * @returns		Retourne la réponse du module visé par la commande
 */
String route(String line)
{
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);
	
	// identification du la vesrion de l'application, également du point d'accès
	if(line == "version")	return	String(version) +"\n";
	
	// liste des commandes possibles
	if(line =="list")	return list;
	if(line =="menu")	return list;

	// Module gestionIO ?	
	// format UDP
	if(Mot[0]== "gpio") return gpioInterpret(line);	

	if(Mot[0]== "i2c") return i2cInterpret(line);	

	if(Mot[0]== "rtc") return rtcInterpret(line);	

	if(Mot[0]== "bme") return BME280Interpret(line);	

	if(Mot[0]== "oled") return oledInterpret(line);

	if(Mot[0]== "ow") return owInterpret(line);

	if(Mot[0]== "ds") return DS1820Interpret(line);

	if(Mot[0]== "dht") return DHT22Interpret(line);	

	if(Mot[0]== "timer") return timerInterpret(line);	

	if(Mot[0]== "conv") return conversionsInterpret(line);	

	if(Mot[0]== "tft") return tftInterpret(line);		

	// commande non reconnue
	return line + "  ???\n";
}

	
