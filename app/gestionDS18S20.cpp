#include <SmingCore/SmingCore.h>
#include <Libraries/OneWire/OneWire.h>
#include "ds18s20.h"
#include <WString.h>
#include "gestionRTC_NTP.h"
#include "gestionOneWire.h"

DS18S20 ReadTemp;
float DS1820[4];
uint8_t dsCount;


// tableaux pour sauvegarder les valeurs
#define qte 24
#define sondeQty 4
float dsTemperature [sondeQty][qte];
String dsTemps[qte];

// Pousse les dernières valeurs du capteurs dans les tableaux
// La plus ancienne valeur des tableaux est écrasée
// Les nouvelles valeurs sont plaçées à la fin
void toHistorique()
{
	int j,i;

	// traitement des mesures
	for(i=0;i<dsCount;i++){	// pour chaque sonde
		for(j=0;j<qte-1;j++){	// pour chaque mesure 
			dsTemperature[i][j]=dsTemperature[i][j+1]; //décale dans le tableau des températures
		}
	}

	for(i=0;i<dsCount;i++){ //pour chaque sonde
		dsTemperature[i][qte-1]=DS1820[i]; //ajout des dernières mesures
	}

	// traitement du temps
	for(i=0;i<qte-1;i++){
		dsTemps[i]=dsTemps[i+1]; // décale
	}
	dsTemps[i]=ts(); // nouveau temps pour la mesure des sondes

}

String DS1820HistoJSON(){
	int i,j;
	String rep = "{\"type\":\"DS1820Histo\",\"ts\":[\"";
	for(i=0;i<qte;i++){
		rep += dsTemps[i];
		if(i<qte-1)rep+="\",\"";
	}
	rep+="\"],\"T\":["; 

	for(j=0;j<dsCount;j++){ // T est un tableau de tableaux
		rep+="[";	
		for(i=0;i<qte;i++){
			rep += String(dsTemperature[j][i],1);
			if(i<qte-1)rep+=",";
		}
		if(j<dsCount-1)rep+="],";	
		else rep+="]";	
	}
	rep+="]}";  
	return rep;
	
}

String DS1820Last(){
	int i;
	String rep = "Dernières valeurs:";
	for(i=0; i<dsCount; i++){
		if(i==2)rep+=" ";
		rep += " T"+String(i+1)+" " + String(DS1820[i],1)+"  ";
	}
	return rep +"\n";
}

/**
 * @brief tranmet les dernières valeurs d'acquisition
 * 
 * @return String 
 */
String DS1820JSON(){
	int i;
	String rep = "{\"type\":\"DS1820\",\"ts\":\""+ts()+"\",\"T\":[";
	for(i=0; i<dsCount; i++){
		rep += String(DS1820[i],1)+",";
	}
	rep.remove(rep.length()-1,1);
	return rep +"]}\n";
}

//**********************************************************
// DS18S20 example, reading
// You can connect multiple sensors to a single port
// (At the moment 4 pcs - it depends on the definition in the library)
// Measuring time: 1.2 seconds * number of sensors
// The main difference with the previous version of the demo:
//  - Do not use the Delay function () which discourages by manufacturer of ESP8266
//  - We can read several sensors
// Usage:
//  Call Init to setup pin eg. ReadTemp.Init(2);   //pin 2 selected
//  Call ReadTemp.StartMeasure();
//   if ReadTemp.MeasureStatus() false read sensors
//   You can recognize sensors by the ID or index.
//***********************************************************

String DS1820Menu(){
	return "\nds menu\nds scan\nds start\nds lire\nds last\nds json\nds histo\n";
}

// utile pour démarrer à distance si la minuterie est inactive
String DS1820Start(){
	ReadTemp.StartMeasure(); 
	return "Démarre la conversion pour les DS1820\n";
}

/**
 * @brief Obtient la valeur de la dernière conversion et démarre une autre
 * 
 * @return String 
 */
String DS1820Lire()
{
	uint8_t a;
	uint64_t info;
	String rep;

	if (!ReadTemp.MeasureStatus())  // the last measurement completed
	{
			//for(a=0;a<4;a++)DS1820[a]=-99.0; // écrase ancienne mesure
	
			dsCount = ReadTemp.GetSensorsCount(); 
			if (dsCount){
				for(a=0;a<dsCount;a++)   // prints for all sensors
				{
					if(a==2)rep+=" ";
					rep += " T"+String(a+1)+" ";
					if (ReadTemp.IsValidTemperature(a))   // temperature read correctly ?
					{
						DS1820[a]=ReadTemp.GetCelsius(a);
						rep += String(DS1820[a],1)+"  ";
					}
					else{
						rep = ("Température non valide\n");
					}
				}

				toHistorique();
				ReadTemp.StartMeasure();  // démarre un autre conversion qui sera obtenu à la prochaine lecture

			}   // is minimum 1 sensor detected ?
	}
	else{
		rep=("Attendre la fin de conversion\n");
	}
	return rep += "\n";
}
void DS18S20init(){
  ReadTemp.Init(OneWirePin);  			// select PIN It's required for one-wire initialization!
	ReadTemp.StartMeasure(); // first measure start,result after 1.2 seconds * number of sensors
}

String DS1820Scan(){
	int a;
	int count = ReadTemp.GetSensorsCount();
	char buf[40];

	// L'union permet de récupérer les 8 octets dans une opération
	// par la suite on extrait les champs désirés
	union {
		uint64_t rom;
		struct {
			uint8_t crc;
			uint8_t serial[6];
			uint8_t type;
		};
	}Rom;

	String rep;

	rep =String(count) + " dispositif(s) sur le bus\n";
	if (count){   // is minimum 1 sensor detected ?
		for(a=0;a<count;a++) {  // prints for all sensors
			//rom[a] = ReadTempGetSensorID(a);
			//Serial.printf("ROM:%lx\n",rom[a]);
	
			Rom.rom = ReadTemp.GetSensorID(a); // récupération des 8 octets
			// séparation de l'info 
			sprintf(buf,"ROM %d T:%x Ser:%x %x %x %x %x %x CRC:%x\n",a,Rom.type,Rom.serial[5],Rom.serial[4],Rom.serial[3],Rom.serial[2],Rom.serial[1],Rom.serial[0],Rom.crc);
			rep+= String(buf);
		}
		rep.toUpperCase();
		Serial.println(rep);
		return rep;
	}
}

String DS1820Interpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return DS1820Menu();		
	}

	if(qteMots == 2){
		if(Mot[1]== "scan")	return DS1820Scan();		
	}

	if(qteMots == 2){
		if(Mot[1]== "start")	return DS1820Start();		
	}

	if(qteMots == 2){
		if(Mot[1]== "last")	return DS1820Last();		
	}

	if(qteMots == 2){
		if(Mot[1]== "lire")	return DS1820Lire();		
	}

	if(qteMots == 2){
		if(Mot[1]== "json")	return DS1820JSON();		
	}

	if(qteMots == 2){
		if(Mot[1]== "histo")	return DS1820HistoJSON();		
	}

	
    return line + ("???\n");	
}


