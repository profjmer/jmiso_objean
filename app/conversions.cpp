// conversions.cpp
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>

String conversionsMenu(){ return "\nconv menu\nconv hextobin 00..FF\nconv dectobin 0..255\n";}

// retourne une chaine contenant les valeurs binaires d'un octet 
// La chaine est séparée en 2 nibbles, MSnib et LSnib
String byteToBin(uint8_t octet){
  int i;
  uint8_t mask;
  char rep[10];
	mask =0x80;
	for(i=0;i<8;i++){
		if((octet & mask) ==0)	rep[i]='0';
		else rep[i]='1';
		mask = mask >> 1;
	}

	for(i=7;i>3;i--){
		rep[i+1]=rep[i];	// décale le high nibble
	}
	rep[4]=' ';	// insère séparateur
	rep[9]=0;	// fin de chaine de la conversion
	return String(rep);
}

// interpréteur de commande 
String conversionsInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 3){
		if(Mot[1]== "hextobin")
		{
			int i;
			uint8_t octet;
			octet = (int8_t) strtol(Mot[2].c_str(),NULL,16);
			return byteToBin(octet); 
		}
		
		if(Mot[1]== "dectobin")
		{
			int i;
			uint8_t octet;
			octet = (int8_t) strtol(Mot[2].c_str(),NULL,10);
			return byteToBin(octet); 
		}
	}

	if(qteMots == 2){
		if(Mot[1]== "menu")	return conversionsMenu();		
	}

	return line + ("???\n");	
}