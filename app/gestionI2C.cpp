// gestionI2C.cpp
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>
#include "gestionI2C.h"

#define SCL_PIN 0
#define SDA_PIN 2

void I2Cinit(){
	Wire.pins(SCL_PIN,SDA_PIN); //SCL SDA
	Wire.begin();
}

String i2cPins(){
	return "SDA "+String(SDA_PIN,10)+" SCL "+String(SCL_PIN,10)+"\n";
}

String i2cMenu(){
return "\ni2c menu\ni2c scan\nPour les commandes I2C, les valeurs sont considérées hexadécimales,\
 sauf pour qty qui est considérée décimale\ni2c read adresseI2C adresseReg qty\ni2c write adresseI2C adresseReg octet\ni2c pins\n";
}

// retourne une String comprenant les esclaves trouvés
String scanBus(){
  uint8_t error, address;
  int nDevices;
  char rep[250];
  String res;

  //Serial.println("Scanning Bus I2C...");

  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
		// The i2c_scanner uses the return value of
		// the Write.endTransmisstion to see if
		// a device did acknowledge to the address.
		Wire.beginTransmission(address);
		error = Wire.endTransmission();

		WDT.alive(); // Second option: notify Watch Dog what you are alive (feed it)

		if (error == 0)
		{
			//Serial.printf("I2C device found at address 0x%#x\n",address);
			sprintf(rep,"I2C esclave  0x%#x\n",address);
			res += rep;

			nDevices++;
		}
		else if (error==4)
		{
			//Serial.println("erreur 4");
			return "erreur 4";
		}
  }
  //Serial.println("Scan done");
  if (nDevices == 0)
	 // Serial.println("no I2C device found");
	 return "Aucun d'esclave I2C trouvé\n";
  else{
	//Serial.println("done");
  
	 return res +"Recherche terminée\n";
  }
}

// lire le contenu des registres débutant à address du slave I2CdeviceAddress, la quantité à lire est length
String readI2C(uint8_t I2CdeviceAddress, uint8_t address, uint8_t length)
{
  uint8_t RxBufferI2C[64];
  int i;
  String res;
  
  if(length > 32)return  "qté <= 32\n";
  Wire.beginTransmission(I2CdeviceAddress);
  Wire.write(address);
  Wire.endTransmission();
  
  Wire.beginTransmission(I2CdeviceAddress);
  Wire.requestFrom(I2CdeviceAddress, length);

  while(Wire.available())
  {
	  for(i = 0; i < length; i++)
	  {
		  RxBufferI2C[i] = Wire.read();
	  }
  }
  Wire.endTransmission();
 
  // tableau de char en string
  for(uint8_t i=0;i<length;i++){
	res += " " +String(RxBufferI2C[i],16); // ff
  }
  res += "\n";
  res.toUpperCase();
  return res;
}

// écrire au slave à I2CdeviceAddress au registre address l'octet de data fourni
String writeI2C(uint8_t I2CdeviceAddress, uint8_t registerID, uint8_t data)
{
	Wire.beginTransmission(I2CdeviceAddress);
	Wire.write(registerID);
	Wire.write(data);
	Wire.endTransmission();
    String rep = ("Écris 0x"+String(data,16) +" adresse 0x" + String(registerID,16) + " esclave 0x" + String(I2CdeviceAddress,16) +"\n");
	return rep;	
}

String writeI2Carray(uint8_t I2CdeviceAddress,uint8_t registerID,uint8_t *p, uint8_t qty)
{
	int i;
	Wire.beginTransmission(I2CdeviceAddress);
	Wire.write((uint8_t) registerID);
	for(i=0; i<qty;i++){
		Wire.write(*p++);
	}
	Wire.endTransmission();
	String rep = ("Écris "+String(qty,10) +" bytes, addresse 0x" + String(registerID,16) + " esclave 0x" + String(I2CdeviceAddress,16) +"\n");
	return rep;		
}

String i2cInterpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "scan")	return scanBus();		
	}

	if(qteMots == 2){
		if(Mot[1]== "pins")	return i2cPins();		
	}

	if(qteMots == 2){
		if(Mot[1]== "menu")	return i2cMenu();		
	}
	
	// lire une qty d'octets à partir de l'adresse registerID du slave I2CdeviceAddress
	if(qteMots == 5){
		if(Mot[1]== "read")
		{	
			uint8_t I2CdeviceAddress = strtol(Mot[2].c_str(),NULL,16);
			uint8_t registerID = strtol(Mot[3].c_str(),NULL,16);
			uint8_t qty = strtol(Mot[4].c_str(),NULL,10);
			String rep = "Read I2C 0x"+Mot[2] +"  start 0x"+Mot[3]+"  Qty "+Mot[4]+"  HEX->";
			rep += readI2C(I2CdeviceAddress, registerID,qty);
			return rep;
		}
	}
	
	// écrire un octet à l'adresse registerID du dispositif I2C à l'adresse I2CdeviceAddress
	if(qteMots == 5){
		if(Mot[1]== "write")
		{	uint8_t I2CdeviceAddress = strtol(Mot[2].c_str(),NULL,16);
			uint8_t registerID = strtol(Mot[3].c_str(),NULL,16);
			uint8_t data = strtol(Mot[4].c_str(),NULL,16);
			String rep = writeI2C(I2CdeviceAddress,registerID, data);
			return rep;
		}
	}

		return line + ("???\n");

}
