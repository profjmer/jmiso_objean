// gestionBME280.cpp
#include <user_config.h>
#include <SmingCore/SmingCore.h>
#include <WString.h>
#include <stdio.h>
#include "Adafruit_BME280.h"
#include "gestionRTC_NTP.h"

Adafruit_BME280 bme;
String t,p,h;
String BME280Time;

// tableaux pour sauvegarder les valeurs
#define qte 24
float bmeTemperature[qte], bmePression[qte], bmeHumidity[qte];
String bmeTemps[qte];

// Pousse les dernières valeurs du capteurs dans les tableaux
// La plus ancienne valeur des tableaux est écrasée
// Les nouvelles valeurs sont plaçées à la fin
void toHistorique(String Temps, float Temperature, float Humidity, float Pression)
{
	int i;
	for(i=0;i<qte-1;i++){
		bmeTemps[i]= bmeTemps[i+1];
		bmeTemperature[i]=bmeTemperature[i+1];
		bmeHumidity[i]=bmeHumidity[i+1];
		bmePression[i]=bmePression[i+1];        
	}
	bmeTemps[i]= Temps;
	bmeTemperature[i]=Temperature;
	bmeHumidity[i]=Humidity;
	bmePression[i]=Pression;    
	//printHistorique();
}

String BME280HistoJSON(){
	int i;
	String rep = "{\"type\":\"BME280Histo\",\"ts\":[\"";
	for(i=0;i<qte;i++){
		rep += bmeTemps[i];
		if(i<qte-1)rep+="\",\"";
	}
	rep+="\"],\"T\":[";
	
	for(i=0;i<qte;i++){
		rep += String(bmeTemperature[i],1);
		if(i<qte-1)rep+=",";
	}
	rep+="],\"HR\":[";
	
	for(i=0;i<qte;i++){
		rep += String(bmeHumidity[i],0);
		if(i<qte-1)rep+=",";		
	} 

	rep+="],\"P\":[";

  for(i=0;i<qte;i++){
		rep += String(bmePression[i],1);
		if(i<qte-1)rep+=",";		
	} 

	rep+="]}";  
	return rep;
	
}

String BME280Menu(){
    return "\nbme menu\nbme lire\nbme last\nbme json\nbme histo\n";
}

void BME280init(){
	if (!bme.begin(0x76)) {  
		Serial.println("BME280 ?, check wiring!");
	}
}

// lance une acquisition et retourne résultat pour client UDP
String BME280Lire(){
    float temp,pression,humid;
    temp = bme.readTemperature();
    pression = bme.readPressure()/1000;
    humid = bme.readHumidity();
    t = String(temp,1);
    p = String(pression,1);
    h = String(humid,0);   
    toHistorique(ts(),temp, humid, pression);
    return "T "+t+" *C  P "+p+" kPa  H "+h+" %";
}

// lance une acquisition et retourne résultat pour client UDP
String BME280JSON(){
    t = String(bme.readTemperature(),1);
    p = String(bme.readPressure()/1000,1);
    h = String(bme.readHumidity(),0);   
    //return "T "+t+" *C  P "+p+" kPa  A "+a+" m\n";
    return "{\"type\":\"BME280\",\"ts\":\""+ts()+"\",\"T\":" +t+",\"P\":"+p+",\"H\":"+h+"}\n";
}

// retourne les résultats de la dernière acquisition pour OLED
String BME280Last(){
    return "T"+t+" P"+p+" H"+h+"\n";
}

void printBME280_data(){   
    Serial.println(BME280Last());
}

String BME280Interpret(String line){
	// Permet de séparer les mots d'une line de caractères
	Vector<String> Mot;
	int qteMots = splitString(line, ' ' , Mot);

	if(qteMots == 2){
		if(Mot[1]== "menu")	return BME280Menu();		
	}
	    
	if(qteMots == 2){
		if(Mot[1]== "last")	return BME280Last();		
	}

    if(qteMots == 2){
		if(Mot[1]== "lire")	return BME280Lire();		
	}

    if(qteMots == 2){
		if(Mot[1]== "json")	return BME280JSON();		
	}

    if(qteMots == 2){
		if(Mot[1]== "histo")	return BME280HistoJSON();		
	}

    return line + ("???\n");	
}

