//udpserver.cpp
#include <SmingCore/SmingCore.h>
#include "routeur.h"

void onReceive(UdpConnection& connection, char *data, int size, IPAddress remoteIP, uint16_t remotePort); // Declaration

// UDP server
const uint16_t UDP_Port = 8266;
UdpConnection udp(onReceive);

void udpserver_start()
{
	udp.listen(UDP_Port);
}

void onReceive(UdpConnection& connection, char *data, int size, IPAddress remoteIP, uint16_t remotePort)
{	
	String line(data);

	// commande transmise automatiquement par le client UDP de jmSerialCom 
	// pour fin d'identification unique, l'adresse MAC du ESP8266 est retournée
	if(line == "ESP:ServerID?\n")
	{
		udp.sendStringTo(remoteIP,remotePort,"ESP8266 Mac:"+WifiAccessPoint.getMAC() + "\n");
		return;
	}
	
	// transmet la ligne de commande à l'interpréteur
	udp.sendStringTo(remoteIP,remotePort, route(line));
}
	
