// gestionRTC_NTP.h

#ifndef INCLUDE_gestionRTC_H_
#define INCLUDE_gestionRTC_H_
	String writeRTC(String dateHeure);
	String reqNTP();
	String readRTC();
	String rtcMenu();
	String rtcInterpret(String line);
	String ts();	
	
#endif //INCLUDE_gestionRTC_H_


	