// gestionOneWire.h

#ifndef INCLUDE_gestionOneWire_H_
#define INCLUDE_gestionOneWire_H_

  #define OneWirePin 16
  void OneWireInit();
  String owMenu();
  String owPresence();
  String scanOW();
  String owInterpret(String line);

#endif //INCLUDE_gestionOneWire_H_

