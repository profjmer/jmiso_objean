// gestionDHT22.h

#ifndef INCLUDE_gestionDHT22_H_
#define INCLUDE_gestionDHT22_H_
	#include <Libraries/DHT/DHT.h>

	void DHT22init();
	String DHT22Lire();
	String DHT222Last();
	String DHT22Menu();
	String DHT22JSON();
	float DHT22Temp();
	float DHT22HR();
	String DHT22Interpret(String line);	
	
#endif //INCLUDE_gestionDHT22_H_

