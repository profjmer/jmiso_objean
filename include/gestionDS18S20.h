// gestionDS18S20.h

#ifndef INCLUDE_gestionDS18S20_H_
#define INCLUDE_gestionDS18S20_H_
	void DS18S20init();
	String DS1820Menu();
	String DS1820Start();
	String DS1820Lire();
	String DS1820Scan();
	String DS1820Last();
	String DS1820JSON();
	String DS1820Interpret(String line);
#endif //INCLUDE_gestionDS18S20_H_

