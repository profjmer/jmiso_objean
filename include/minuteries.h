#ifndef INCLUDE_MINUTERIES_H_
#define MINUTERIESstionI2C_H_

    void timerInit();
    String timerBME280stop();
    String timerBME280restart();
    String timerDHT22stop();
    String timerDHT22restart();
    String timerDS1820stop();
    String timerDS1820restart();
    String timerMenu();
    String timersStop();
    String timersRestart();
    String timerInterpret(String line);
#endif //MINUTERIESstionI2C_H_