// gestionOLED.h
#ifndef INCLUDE_gestionOLED_H_
#define INCLUDE_gestionOLED_H_

	void OLEDinit();
	void updateOLED();
	void setZone1(String);
	void setZone2(String);
	void setZone3(String);
	void setZone4(String);
	String oledMenu();
	String oledInterpret(String line);

#endif //INCLUDE_gestionOLED_H_




