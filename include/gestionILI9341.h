// gestionILI9341.h

#ifndef __ILI9341BME280_H__
	#define __ILI9341_H__
		
	void tftInit();
  String tftMenu();
  String tftInterpret(String line);
  void tftIntro();
  void simulation();
	
#endif