// gestionBME280.h

#ifndef __BME280_H__
	#define __BME280_H__
		
	void BME280init();
	String BME280Lire();
	String BME280Last();
	String BME280Menu();
	String BME280JSON();
	String BME280HistoJSON();	
	String BME280Interpret(String line);
	void printBME280_data();	
	
#endif
