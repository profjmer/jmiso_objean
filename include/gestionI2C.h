// gestionI2C.h

#ifndef INCLUDE_gestionI2C_H_
#define INCLUDE_gestionI2C_H_
	
	// détermine les broches I2C et initialise le bus
	void I2Cinit();

	// retourne les commandes du module
	String i2cMenu();
	
	// retourne une String comprenant les esclaves trouvés
	String scanBus();

	// écrire à l'adresseI2C dans le registre à l'adresseReg un octet de data
	String writeI2C(uint8_t I2CdeviceAddress, uint8_t address, uint8_t data);

	// écrire le contenu d'un tableau d'octets à partir de registerID
	String writeI2Carray(uint8_t I2CdeviceAddress,uint8_t registerID,uint8_t *p, uint8_t qty);

	// lire le contenu des registres débutant à address du slave I2CdeviceAddress, la quantité à lire est length
	String readI2C(uint8_t I2CdeviceAddress, uint8_t address, uint8_t length);

	// interpréteur de la ligne de commande
	String i2cInterpret(String line);
	
#endif //INCLUDE_gestionI2C_H__