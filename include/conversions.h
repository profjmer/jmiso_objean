// conversions.h
#ifndef __CONVERSIONS_H__
	#define __CONVERSIONS_H__
		
  String conversionsMenu();
  String byteToBin(uint8_t octet);
  String conversionsInterpret(String line);
	
#endif //__CONVERSIONS_H__

