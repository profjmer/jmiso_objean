// Initialize WebSocket connection and event handlers
function setupWebsocket() {
	var ctx = document.getElementById('canvas').getContext('2d');
	var wsUri = "ws://" + location.host + "/"; // pour ESP8266
	ws = new WebSocket(wsUri);

	var qtePoints =24; // qte de points du graphe à l"écran
		
	ws.onopen = function(e) {
		console.log("Client en ligne,requête historique");
		// requête pour dernières valeurs
		ws.send("ds histo\n");
	}

	// Listen for the close connection event
	ws.onclose = function(e) {
		log("WS hors ligne : " + e.reason);
	}

	// Listen for connection errors
	ws.onerror = function(e) {
		log("Erreur ");
	}

	// Listen for new messages arriving at the client
	ws.onmessage = function(e) {
		log("Message: " + e.data);
		
		if (typeof e.data === "string") {

			try {
				// Create a JSON object.
				var jsonObject = JSON.parse(e.data);
			
				if(jsonObject.type=="DS1820"){
					
					// Extraire les valeurs des clés
					var temp = jsonObject.T;
					var temps = jsonObject.ts; 

					// si le nombre d'élément dans le dataset est maximun
					// retirer le plus ancien.
				
					for(i=0;i<lineChartData.datasets.length;i++){
						if(lineChartData.datasets[i].data.length >=qtePoints)lineChartData.datasets[i].data.shift();
						
						lineChartData.datasets[i].data.push({x: temps,y: temp[i]});					
					}

					window.myLine.update();

				}

				if(jsonObject.type=="DS1820Histo"){
					//log("Message received: " + e.data);
					// Extraire les valeurs des clés
					var temps = jsonObject.ts;
					var temp = jsonObject.T;

					// copie des valeurs dans les datasets
					for(i=0;i<lineChartData.datasets.length;i++){
						for(j=0;j<temps.length;j++){
							lineChartData.datasets[i].data[j]={"x":temps[j],"y":temp[i][j]};	
						}						
					}

					window.myLine.update();			
			}
				
			} catch (error) {
				log("data pas JSON");			
			}

		}
	}
}

// Display logging information in the document.
function log(s) {
	console.log(s);
}



