// Initialize WebSocket connection and event handlers
function setupWebsocket() {
	var wsUri = "ws://" + location.host + "/"; // pour ESP8266
	ws = new WebSocket(wsUri);
			
	ws.onopen = function(e) {
		console.log("Client en ligne,requête lastDHT22");
		// requête pour dernières valeurs
		ws.send("dht json\n");
	}
	// Listen for the close connection event
	ws.onclose = function(e) {
		log("WS hors ligne: " + e.reason);
	}
	// Listen for connection errors
		ws.onerror = function(e) {
		log("Error ");
	}
	// Listen for new messages arriving at the client
	ws.onmessage = function(e) {		
		if (typeof e.data === "string") {
			log(e.data);
			try {
				// Create a JSON object.
				var jsonObject = JSON.parse(e.data);
				switch(jsonObject.type ){
					case 'dht22':
						var ts = jsonObject.ts;
						var temp = jsonObject.T;
						var humidity = jsonObject.HR;

						document.getElementById("time").innerHTML = ts;
						var gaugeT = document.gauges.get('temperature');
						gaugeT.value = temp;
						var gaugeRH = document.gauges.get('humidity');
						gaugeRH.value = humidity;
					break;
				}
				
			} catch (error) {
				log("data pas JSON");				
			}
		}
		else log("data pas une String");
	}

	// Send a message on the WebSocket.
	function sendMessage(msg){
		ws.send(msg);
		log("Message transmis");
	}
	// Display logging information in the document.
	function log(s) {
		console.log(s);
	}
}