// Initialize WebSocket connection and event handlers
function setupWebsocket() {	
	var wsUri = "ws://" + location.host + "/"; // pour ESP8266
    ws = new WebSocket(wsUri);
		
	ws.onopen = function(e) {
	console.log("Client en ligne, requête ds json");
	// requête des dernières valeurs
	ws.send("ds json\n");
	}

	// Listen for the close connection event
	ws.onclose = function(e) {
		log("Client parti: " + e.reason);
	}

	// Listen for connection errors
		ws.onerror = function(e) {
		log("Error ");
	}

	// Listen for new messages arriving at the client
	ws.onmessage = function(e) {
		log("Message Rx: " + e.data);
		
		if (typeof e.data === "string") {
			try {
				// Create a JSON object.
				var jsonObject = JSON.parse(e.data);
				switch(jsonObject.type) {
				case 'text':
					console.log("Rx Texte: " + jsonObject.content)
					break;
				
				case "DS1820":	
					// Extraire le tableau des valeurs de température
					var ts = jsonObject.ts;
					var T = jsonObject.T;

					document.getElementById("time").innerHTML = ts;					
					document.getElementById("ds1").innerHTML = T[0];
					document.getElementById("ds2").innerHTML = T[1];
					document.getElementById("ds3").innerHTML = T[2];
					document.getElementById("ds4").innerHTML = T[3];			
				}   
				
			} catch (error) {
				log("data pas JSON");		
			}	
		}
		else log("data pas une String");
	}

	// Send a message on the WebSocket.
	function sendMessage(msg){
		ws.send(msg);
		log("Message transmis");
	}

	// Display logging information in the document.
	function log(s) {
		console.log(s);
	}
}

