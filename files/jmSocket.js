function startWebsocket() {
  var wsUri = "ws://" + location.host + "/";
  ws = new WebSocket(wsUri);
  ws.onopen = function(evt) { 
    console.log('websock open'); 
    ws.send("gpio\n");
  };
  ws.onclose = function(evt) {
    document.getElementById("output").innerHTML="Socket non connecté";	
    console.log('websock close'); 
  };
  ws.onerror = function(evt) { console.log(evt); };
  ws.onmessage = function(evt) { 
    console.log(evt.data);
    var ligne = evt.data;
    var ui= ligne.replace(/\n/g,"")
    ligne = ligne.replace(/\n/g,"<br>")
    
    document.getElementById("output").innerHTML=ligne;	
   
    // traitement de la réponse pour modifier la page
    Mots = ui.split(' ');
    if(Mots[0]=="GPIO"){
      switch(Mots[1]){
        case "0":						
          if(Mots[2]=="0") document.getElementById("gpio0").src = "./delJaune.png";
          if(Mots[2]=="1") document.getElementById("gpio0").src = "./delOff.png";
        break;

        case "2":						
        if(Mots[2]=="0") document.getElementById("gpio2").src = "./delBleu.png";
        if(Mots[2]=="1") document.getElementById("gpio2").src = "./delOff.png";
      break;
      }
    }
  };	

  commande.addEventListener('keypress', keyGotPressed, false);
  function keyGotPressed (e){
    var charCode = e.which;
    var cmd = document.getElementById('commande');
    if(charCode==13){
        console.log(cmd.value);
        ws.send(cmd.value);
    }
  };
};

// envoi de la commande de basculer l'état d'un GPIO
function flip(id) {
    ws.send("gpio flip "+id.substring(4) +"\n");
}
