// Initialize WebSocket connection and event handlers
function setupWebsocket() {	
	var wsUri = "ws://" + location.host + "/"; // pour ESP8266
  ws = new WebSocket(wsUri);
		
	ws.onopen = function(e) {
	console.log("Client en ligne, requête bme json");
	// requête pour dernières valeurs
	ws.send("ds json\n");
	}

	// Listen for the close connection event
	ws.onclose = function(e) {
		log("Client parti: " + e.reason);
	}

	// Listen for connection errors
		ws.onerror = function(e) {
		log("Error ");
	}

	// Listen for new messages arriving at the client
	ws.onmessage = function(e) {
		log("Message Rx: " + e.data);
		
		if (typeof e.data === "string") {
			// Create a JSON object.
			try {
				var jsonObject = JSON.parse(e.data);
				switch(jsonObject.type) {
					case 'text':
						console.log("Rx Texte: " + jsonObject.content)
						break;
					
					case "DS1820":	
						// Extraire le tableau des valeurs de température
						var ts = jsonObject.ts;
						var temperature = jsonObject.T;


						document.getElementById("time").innerHTML = ts;

						var DS1 = document.gauges.get('ds1');
						DS1.value = temperature[0];

						var DS2 = document.gauges.get('ds2');
						DS2.value = temperature[1];	

						var DS3 = document.gauges.get('ds3');
						DS3.value = temperature[2];	

						var DS4 = document.gauges.get('ds4');
						DS4.value = temperature[3];							

						break;			
				}  
				
			} catch (error) {
				log("data pas JSON");		
			}
		}
		else log("data pas une String");
	}

	// Send a message on the WebSocket.
	function sendMessage(msg){
		ws.send(msg);
		log("Message transmis");
	}

	// Display logging information in the document.
	function log(s) {
		console.log(s);
	}
}


